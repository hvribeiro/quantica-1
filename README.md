# Quântica 1

*Notas de aula do curso de Quântica 1 - PFI/UEM*

Vamos usar o livro [**Mecânica Quântica Moderna, J. J. Sakurai, J. Napolitano**](http://93.174.95.29/main/1B202AA67B458689652A75E05547FC25).

Lista de problemas disponível [aqui](http://complex.pfi.uem.br/ribeiro/listas/pos_quantica_i.pdf).


## Parte 1 - Conceitos Fundamentais

- [Aula 01-02 - 13/09/2021](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/parte1/aula-01-02.png) | [Vídeo](https://drive.google.com/file/d/1-mn-tpGk6zVuJLYh0dLFHKTmORZIvXvf/view?usp=drive_web&authuser=1)
- [Aula 03-04 - 20/09/2021](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/parte1/aula-03-04.png) | [Vídeo](https://drive.google.com/file/d/14ZYFEjtLIpTR4aSf2iLDw6XOZcjP71uB/view?usp=drive_web&authuser=1)
- [Aula 05-06 - 27/09/2021](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/parte1/aula-05-06.png) | [Vídeo](https://drive.google.com/file/d/1CaNJ8PaZDHWDpgvVxIyosSzYUtwNxYEJ/view?usp=drive_web&authuser=1)
- [Aula 07-08 - 04/10/2021](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/parte1/aula-07-08.png) | [Vídeo](https://drive.google.com/file/d/1WbaghDrZ0mK6Kc2KYa2u7vKBcuYe2Uin/view?usp=drive_web&authuser=1)
- [Aula 09-10 - 18/10/2021](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/parte1/aula-09-10.png) | [Vídeo](https://drive.google.com/file/d/1QqRtGRKyGtVgN4beiFFBDRgR3oAVacqL/view?usp=drive_web&authuser=1)


## Parte 2 - Dinâmica Quântica

- [Aula 11-12 - 25/10/2021](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/parte2/aula-11-12.png) | [Vídeo](https://drive.google.com/file/d/1MosgvbM5GCal1X5RYWumc2OcE-P00Dhp/view?usp=drive_web&authuser=1)
- [Aula 13-14 - 01/11/2021](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/parte2/aula-13-14.png) | [Vídeo](https://drive.google.com/file/d/1Ms-5TM9pOCB8Kq-nBSf24Fcu8tyeOmfb/view?usp=drive_web&authuser=1)
- [Aula 15-16 - 08/11/2021](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/parte2/aula-15-16.png) | [Vídeo](https://drive.google.com/file/d/1Ye4sctqgovac7Ie5D7VgLqz1bLpGpchK/view?usp=drive_web&authuser=1)
- [Aula 17-18 - 22/11/2021](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/parte2/aula-17-18.png) | [Vídeo](https://drive.google.com/file/d/1zKvK7q6X_V9kmVixslqSRmWbPx2ewWfd/view?usp=drive_web&authuser=1)
- [Aula 19-20 - 30/11/2021](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/parte2/aula-19-20.png) | [Vídeo](https://drive.google.com/file/d/1jp8G32Hce5g4yBiBAwv5s1BV_PC8eCSR/view?usp=drive_web&authuser=1)


## Parte 3 - Momento Angular

- [Aula 21-22 - 01/12/2021](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/parte3/aula-21-22.png) | [Vídeo](https://drive.google.com/file/d/1S33FrqZAc8IXaBheugKgrAs1H58x194Q/view?usp=drive_web&authuser=1)
- [Aula 23-24 - 06/12/2021](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/parte3/aula-23-24.png) | [Vídeo](https://drive.google.com/file/d/18ntDeAW74bK7NHC2apDdeaFWdBQAlS8B/view?usp=drive_web&authuser=1)
- [Aula 25-26 - 08/12/2021](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/parte3/aula-25-26.png) | [Vídeo](https://drive.google.com/file/d/1SzG8ahFvrXMZqBBdWXsWxH60l56q7Ruj/view?usp=drive_web&authuser=1)
- [Aula 27-28 - 13/12/2021](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/parte3/aula-27-28.png) | [Vídeo](https://drive.google.com/file/d/11i8Y0FkBp6O3mOORgtHrTLUCw4YgFb_Z/view?usp=drive_web&authuser=1)