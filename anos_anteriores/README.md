# Quântica 1

*Notas de aula do curso de Quântica 1 - PFI/UEM*

Vamos usar o livro [**Mecânica Quântica Moderna, J. J. Sakurai, J. Napolitano**](http://93.174.95.29/main/1B202AA67B458689652A75E05547FC25).

Lista de problemas disponível [aqui](http://complex.pfi.uem.br/ribeiro/listas/pos_quantica_i.pdf).


## Parte 1 - Conceitos Fundamentais

- [Aula 01 - 13/04/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte1/aula_01.png)
- [Aula 02 - 15/04/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte1/aula_02.png)
- [Aula 03 - 20/04/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte1/aula_03.png)
- [Aula 04 - 22/04/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte1/aula_04.png)
- [Aula 05 - 27/04/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte1/aula_05.png)
- [Aula 06 - 29/04/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte1/aula_06.png)
- [Aula 07 - 04/05/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte1/aula_07.png)
- [Aula 08 - 06/05/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte1/aula_08.png)

## Parte 2 - Dinâmica Quântica

- [Aula 09 - 11/05/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte2/aula_09.png)
- [Aula 10 - 13/05/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte2/aula_10.png)
- [Aula 11 - 18/05/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte2/aula_11.png)
- [Aula 12 - 20/05/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte2/aula_12.png)
- [Aula 13 - 25/05/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte2/aula_13.png)
- [Aula 14 - 27/05/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte2/aula_14.png)
- [Aula 15 - 01/06/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte2/aula_15.png)
- [Aula 16 - 03/06/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte2/aula_16.png)
- [Aula 17 - 08/06/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte2/aula_17.png)
- [Aula 18 - 10/06/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte2/aula_18.png)
- [Aula 19 - 15/06/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte2/aula_19.png)
- [Aula 20 - 17/06/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte2/aula_20.png)

## Parte 3 - Momento Angular

- [Aula 21 - 22/06/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte3/aula_21.png)
- [Aula 22 - 24/06/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte3/aula_22.png)
- [Aula 23 - 29/06/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte3/aula_23.png)
- [Aula 24 - 01/07/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte3/aula_24.png)
- [Aula 25 - 06/07/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte3/aula_25.png)
- [Aula 26 - 08/07/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte3/aula_26.png)
- [Aula 27 - 13/07/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte3/aula_27.png)
- [Aula 28 - 15/07/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte3/aula_28.png)
- [Aula 29 - 20/07/2020](https://gitlab.com/hvribeiro/quantica-1/-/blob/master/anos_anteriores/2020/parte3/aula_29.png)